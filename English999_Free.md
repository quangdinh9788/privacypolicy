# Chính sách bảo mật

## Về English999_Free
 <font size="1">English999 phiên bản free là một ứng dụng hỗ trợ học Tiếng Anh, có sẵn 999 câu Tiếng Anh thông dụng được dịch sang Tiếng Việt. Hỗ trợ đọc bằng 3 giọng: Giọng Việt, Giọng Anh và Giọng Anh(Mỹ). Trong ứng dụng có chức năng tạo một câu Tiếng Anh ngẫu nhiên từ danh sách rồi cho phép người dùng tự dịch sang Tiếng Việt hoặc ngược lại.</font>

## Đối tượng và độ tuổi
  <font size="2">Không hạn chế</font>

## Thông tin người dùng
 Ứng dụng hoàn toàn không yêu cầu cũng như thu thập bất cứ thông tin nào của người dùng



# PrivacyPolicy

## About English999_Free
Free version English999 is an application to support learning English, available with 999 common English sentences translated into Vietnamese. Support reading 3 voices: Vietnamese Voice, British Voice, and American Voice. In the application, there is a function to generate a random English sentence from the list and then allow users to manually translate it into Vietnamese or vice versa.

## Audience and age
 No user restrictions

## User information
 The app does not ask or collect any user information at all
